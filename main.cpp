#include <iostream>
using namespace std;
const int tamanho=5;
struct registro{
    char nome[60];
    int idade;
};
struct registro* registros[tamanho];
    void inicializar(registro *registros[], int tamanho){
        for (int i=0; i<tamanho; i++)
        registros[i] = new registro;
}
    void lerRegistro (registro *registros[], int tamanho){
        cout <<"Digite os valores para os registros"<<endl;
        for (int i=0; i<tamanho; i++){
            cout << "Nome: ";
            cin.getline(registros[i]->nome,60);
            cout << "Idade: ";
            cin >> registros[i]->idade;
            cout<<endl;
            cin.ignore();
    }
}
void imprimirRegistros(registro *registros[], int tamanho){
    cout << "Registros cadastrados "<<endl;
    for (int i=0; i<tamanho; i++){
        cout << "Nome: " <<registros[i]->nome <<endl;
        cout << "Idade: " <<registros[i]->idade<<endl;
    }
}
void ordenarBolhaPorIdade(registro *registros[], int tanamho){
    for (int i=tamanho-1; i>0; i--){
        for (int j=0; j<i; j++){
            if(registros[j]->idade>registros[j+1]->idade){
                registro *aux = registros [j];
                registros[j]=registros[j+1];
                registros[j+1]=aux;
            }
        }
    }
}
void buscarSequencialmente(registro *registros[], int tamanho, int idadeAlvo){
    for (int i=0; i<tamanho; i++){
        if (registros[i]->idade == idadeAlvo){
            cout <<"Registro encontrado"<<endl;
            cout <<"Nome: "<<registros[i]->nome<<endl;
            cout <<"Idade: "<<registros[i]->idade<<endl;
            cout<<endl;
            return;
        }
    }
    cout<<"O registro n�o foi encontrado!"<<endl;
}
void buscarBinariamente(registro *registros[], int tamanho, int alvoIdade){
    int inf  = 0;
    int sup = tamanho -1;
    while (inf<=sup){
        int meio = (inf + sup)/2;
        if (registros[meio]->idade==alvoIdade){
            cout << "Nome: "<<registros[meio]->nome<<endl;
            cout << "Idade: "<<registros[meio]->idade<<endl;
            return;
        }else
        if (alvoIdade < registros[meio]->idade){
            sup = meio-1;
        }else{
            inf = meio+1;
        }
    }
    cout<<"Registro n�o foi encontrado!"<<endl;
}
int main(){
    inicializar(registros, tamanho);
    lerRegistro(registros, tamanho);
    ordenarBolhaPorIdade(registros, tamanho);
    imprimirRegistros(registros, tamanho);
    int valor;
    cout<<"Digite a idade de busca ";
    cin >> valor;
    buscarSequencialmente(registros, tamanho, valor);
    buscarBinariamente(registros, tamanho, valor);
    cout << "FIM" <<endl;
    return 0;
}
